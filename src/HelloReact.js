import React, { Component } from "react";
import FormBS3 from "./components/FormBS3";
import Product from "./components/Product";
import Util from "./components/Util";

class HelloReact extends Component {
  constructor(props) {
    super(props);
    this.clickMeRef = React.createRef;
    this.state = {
      click_me_color: "red",
      click_me_color_changed: false,
    };
  }

  onClickMe(a) {
    if (this.state.click_me_color_changed) {
      this.setState({
        click_me_color: "red",
        click_me_color_changed: false,
      });
    } else {
      this.setState({
        click_me_color: "green",
        click_me_color_changed: true,
      });
    }
    // alert(a);
  }

  render() {
    return (
      <React.Fragment>
        <div>
          <div className="page-header">
            <h1>
              Smart Phone<small>(1)</small>
            </h1>
          </div>
          <Product static_note="Het hang">resupplying</Product>
          <Util />
          <button
            type="button"
            className="btn btn-default"
            style={{ backgroundColor: this.state.click_me_color }}
            onClick={() => {
              this.onClickMe("Clicked click me button, yes");
            }}
            ref={this.clickMeRef}
          >
            Click me!
          </button>
        </div>
        <FormBS3 />
      </React.Fragment>
    );
  }
}

export default HelloReact;
