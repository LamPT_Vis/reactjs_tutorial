import React, { Component } from "react";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import Header from "./components/Header";
import routes from "./components/routes";
import HelloReact from "./HelloReact";

class Navigator extends Component {

  showContent = (routes) => {
    var result = null;
    if (routes.length > 0) {
      result = routes.map((route, index) => {
        return (
          <Route key={index} path={route.path} exact={route.isExact} component={route.main}  />
        );
      });
    }
    return result;
  };

  render() {
    return (
      <Router>
        <Header />
        <Switch>
          <Route path='/' exact component={HelloReact}/>
          {this.showContent(routes)}
        </Switch>
      </Router>
    );
  }
}
export default Navigator;
