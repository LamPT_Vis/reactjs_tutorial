import React from "react";

export const ActionType = {
  TRACKS_SET: "TRACKS_SET",
  TRACKS_ADD: "TRACKS_ADD",
};
