import React from 'react';
import {setTracks,addTracks} from './tracks';

export{
    setTracks,
    addTracks
}