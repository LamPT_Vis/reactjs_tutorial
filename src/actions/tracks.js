import React from 'react';
import {ActionType} from './constant';

export function setTracks(tracks){
    return{
        type:ActionType.TRACKS_SET,
        tracks
    };
}

export function addTracks(trackTitle){
    return{
        type: ActionType.TRACKS_ADD,
        title:trackTitle
    };
}