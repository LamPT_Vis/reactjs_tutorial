import React, { Component } from "react";

class FormBS3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txtInput1: "",
      txtInput2: "",
      gender: 0,
      rbCurrency: "VND",
      cbRank1: "0",
      cbRank2: "0",
      cbRank3: "1",
    };
    this.onTxtType = this.onTxtType.bind(this);
    this.onClickSubmit = this.onClickSubmit.bind(this);
  }

  onTxtType(event) {
    var target = event.target;
    var name = target.name;
    var value = target.value;

    this.setState({
      [name]: value,
    });
    console.log(event);
  }

  onClickSubmit(event) {
    event.preventDefault();
    console.log(this.state);
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <div className="panel panel-primary">
              <div className="panel-heading">
                <h3 className="panel-title">Form</h3>
              </div>
              <div className="panel-body">
                <form onSubmit={this.onClickSubmit}>
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      name="txtInput1"
                      onChange={this.onTxtType}
                    />
                  </div>
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      name="txtInput2"
                      onChange={this.onTxtType}
                    />
                  </div>

                  <select
                    className="form-control"
                    name="gender"
                    onChange={this.onTxtType}
                  >
                    <option value={0}>Nam</option>
                    <option value={1}>Nu</option>
                  </select>
                  <br />

                  <div className="radio">
                    <label>
                      <input
                        type="radio"
                        name="rbCurrency"
                        value="VND"
                        onChange={this.onTxtType}
                        checked={this.state.rbCurrency === "VND"}
                      />
                      VND
                    </label>
                    <br />
                    <label>
                      <input
                        type="radio"
                        name="rbCurrency"
                        value="USD"
                        onChange={this.onTxtType}
                        checked={this.state.rbCurrency === "USD"}
                      />
                      USD
                    </label>
                    <br />
                    <label>
                      <input
                        type="radio"
                        name="rbCurrency"
                        value="YEN"
                        onChange={this.onTxtType}
                        checked={this.state.rbCurrency === "YEN"}
                      />
                      YEN
                    </label>
                    <br />
                  </div>

                  <div className="checkbox">
                    <label>
                      <input
                        type="checkbox"
                        name="cbRank1"
                        value={1}
                        onChange={this.onTxtType}
                        
                      />
                      Casual
                    </label>
                    <br />
                    <label>
                      <input
                        type="checkbox"
                        name="cbRank2"
                        value={2}
                        onChange={this.onTxtType}
                        
                      />
                      VIP
                    </label>
                    <br />
                    <label>
                      <input
                        type="checkbox"
                        name="cbRank3"
                        value={3}
                        onChange={this.onTxtType}
                      />
                      Platinum
                    </label>
                    <br />
                  </div>

                  <button type="submit" className="btn btn-primary">
                    Submit
                  </button>
                  <button type="reset" className="btn btn-default">
                    Reset
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FormBS3;
