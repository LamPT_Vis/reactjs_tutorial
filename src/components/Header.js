import React, { Component } from "react";
import { NavLink, Route, Router } from "react-router-dom";

const Menu = ({ label, to, showExact }) => {
  return (
    <Route
      path={to}
      exact={showExact}
      children={({ math }) => {
        var className = math ? "active abc" : "";
        return (
          // su dung nhieu className = cu phap ES6
          <li className={`my-li ${className}`}>
            <NavLink to={to}>{label}</NavLink>
          </li>
        );
      }}
    ></Route>
  );
};

class Header extends Component {
  render() {
    return (
      <React.Fragment>
        <nav className="navbar navbar-inverse">
          <a className="navbar-brand">Title</a>
          <ul className="nav navbar-nav">
            <li>
              <NavLink
                to='/'
                exact
                activeStyle={{ backgroundColor: "white", color: "black" }}
              >
                HelloReact Page
              </NavLink>
            </li>
            <li>
              <NavLink
                to='/songs'
                activeStyle={{ backgroundColor: "white", color: "black" }}
              >
                List songs
              </NavLink>
            </li>
            <li>
              <NavLink
                to='/songsReflect'
                activeStyle={{ backgroundColor: "white", color: "black" }}
              >
                Same state LS
              </NavLink>
            </li>
          </ul>
        </nav>

        <div className="navbar">
          <a className="navbar-brand" href="#">
            Title
          </a>
          <ul className="nav navbar-nav">
            <Menu to="/" label="Hello React page" showExact={true}></Menu>
            <Menu to="/songs" label="List songs" showExact={false}></Menu>
            <Menu to="/songsReflect" label="Same state LS" showExact={false}></Menu>
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

export default Header;
