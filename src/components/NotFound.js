import React, { Component } from 'react';


class NotFound extends Component{
    render(){
        return(
            <div className="row">
                <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <h1>404 not found</h1>
                </div>
            </div>
            
        );
    }
}
export default NotFound;