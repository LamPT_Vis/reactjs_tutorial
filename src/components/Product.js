import React, { Component } from "react";
import { NavLink, Prompt } from "react-router-dom";
import ProductDetail from "./ProductDetail";

class Product extends Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
  }

  onClickDetail = () => {
    alert(this.props.children);
  };

  onSubmit = () => {
    alert(this.textInput.current.value);
  };

  render() {
    var pride = 15000000;

    var mobiles = [
      {
        id: 1,
        imgs:
          "https://www.xtmobile.vn/vnt_upload/product/Hinh_DT/Iphone/ip_X/thumbs/(600x600)_crop_iphone-x-64gb-gray-xtmobile.jpg",
        alt: "Iphone-x",
        pride: 15000000,
        des: "64GB Black",
      },
      {
        id: 2,
        imgs:
          "https://www.xtmobile.vn/vnt_upload/product/10_2020/thumbs/600_samsung_galaxy_a71_xanh.jpg",
        alt: "samsung_galaxy_a71_xanh",
        pride: 7000000,
        des: "samsung_galaxy_a71_xanh",
      },
      {
        id: 3,
        imgs:
          "https://cdn.tgdd.vn/Products/Images/42/193528/bphone-3-pro-15-600x600.jpg",
        alt: "bphone-3-pro",
        pride: 10000000,
        des: "bphone-3-pro",
      },
      {
        id: 4,
        imgs:
          "https://cdn.tgdd.vn/Products/Images/42/228531/vsmart-aris-pro-green-600jpg-600x600.jpg",
        alt: "vsmart-aris-pro-green",
        pride: 8000000,
        des: "vsmart-aris-pro-green",
      },
    ];

    let elements = mobiles.map((mobile, index) => {
      return (
        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4" key={index}>
          <div className="thumbnail">
            <img src={mobile.imgs} alt={mobile.alt} />
            <div className="caption">
              <h3>{mobile.pride}</h3>
              <p>{mobile.des}</p>
              <p>
                {/* <a
                  href="#"
                  className="btn btn-primary"
                  onClick={this.onClickDetail}
                >
                  Detail
                </a> */}
                <NavLink
                  to={`/detail/${mobile.alt}`}
                  className="btn btn-primary"
                >
                  Detail
                </NavLink>
                <Prompt
                  when={true}
                  message={(location) =>
                    `Xac nhan chuyen den : ${location.pathname}`
                  }
                ></Prompt>
                <a href="#" className="btn btn-default">
                  Add to cart
                </a>
              </p>
              <a> {this.props.static_note} </a>
              <p> {this.props.children} </p>
            </div>
          </div>
        </div>
      );
    });

    return (
      <div className="row">
        <div
          className="col-xs-9 col-sm-9 col-md-9 col-lg-9"
          style={{ paddingLeft: "30px", paddingBottom: "30px" }}
        >
          <div className="panel panel-primary">
            <div className="panel-heading">
              <h3 className="panel-title">Search</h3>
            </div>
            <div className="panel-body">
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  ref={this.textInput}
                />
              </div>
              <button
                type="submit"
                className="btn btn-primary"
                onClick={this.onSubmit}
              >
                Submit
              </button>
            </div>
          </div>
        </div>

        {elements}
      </div>
    );
  }
}

export default Product;
