import React, { Component } from 'react';


class ProductDetail extends Component{

    render(){

        var {match}=this.props;
        var name=match.params.alt;

        return(
            <div className="row">
                <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <h1>Product detail: {name}</h1>
                </div>
            </div>
            
        );
    }
}
export default ProductDetail;