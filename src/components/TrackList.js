import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import * as actions from '../actions';

class TrackList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txtTitle: "",
    };
  }

  static propTypes = {
    tracks: PropTypes.array,
  };

  static defaultProps = {
    tracks: [],
  };

  onChange=(event)=> {
    var target = event.target;
    var value = target.value;
    this.setState({
        txtTitle:value
    })
  }

  onSubmit=(event)=>{
      event.preventDefault();
      this.props.dispatch(actions.addTracks(this.state.txtTitle));
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <div className="panel panel-primary">
              <div className="panel-heading">
                <h3 className="panel-title">Tracks list</h3>
              </div>

              <div className="panel-body">
                <form onSubmit={this.onSubmit}>
                  <legend>Them bai hat:</legend>

                  <div className="form-group">
                    <label>Title:</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="ten bai hat"
                      name="txtTitle"
                      onChange={this.onChange}
                    />
                  </div>
                  <button type="submit" className="btn btn-primary">
                    Add
                  </button>
                </form>
                <br />
                <ul className="list-group">
                  {this.props.tracks.map((track, index) => {
                    return (
                      <li key={index} className="list-group-item">
                        {track.title}
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const tracks = state.tracks;
  return { tracks };
}

export default connect(mapStateToProps)(TrackList);


//note:
//https://react-redux.js.org/using-react-redux/connect-mapdispatch
//https://redux.js.org/api/store