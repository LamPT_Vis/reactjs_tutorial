import React, { Component } from "react";

class Util extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text_size: "8px",
      init_size_id: 0,
      text_color: 'black',
      list_text_size: [
        {
          value: "8px",
        },
        {
          value: "10px",
        },
        {
          value: "12px",
        },
        {
          value: "14px",
        },
        {
          value: "16px",
        },
      ],
      colors:[
        {
            value:'red',
        },
        {
            value:'orange'
        },
        {
            value:'yellow'
        }
    ]
    };
  }

  showStatus(something) {
    if (something.status) {
      return (
        <h3
          style={{
            fontSize: this.state.text_size,
            color: this.state.text_color,
          }}
        >
          Function showStatus return value: OK.
        </h3>
      );
    } else {
      return (
        <h3
          style={{
            fontSize: this.state.text_size,
            color: this.state.text_color,
          }}
        >
          Function showStatus return value: NOPE.
        </h3>
      );
    }
  }

  onUpSize = () => {
    if (this.state.init_size_id != 4) {
      this.setState({
        text_size: this.state.list_text_size[this.state.init_size_id + 1].value,
        init_size_id: this.state.init_size_id + 1,
      });
    }
  };

  onDownSize = () => {
    if (this.state.init_size_id != 0) {
      this.setState({
        text_size: this.state.list_text_size[this.state.init_size_id - 1].value,
        init_size_id: this.state.init_size_id - 1,
      });
    }
  };

  onChangColor(index){
      this.setState({
        text_color: this.state.colors[index].value
      })
  }

  render() {
    var something = {
      id: 1,
      name: "ble ble",
      value: 12345,
      status: false,
    };

    let colorButtons=this.state.colors.map((button,index)=>{
        return(
            <button
              type="button"
              className="btn btn-danger"
              style={{backgroundColor:button.value}}
              onClick={()=>this.onChangColor(index)}
              key={index}
            >
            </button>
        );
    })

    return (
      <div>
        {/* React.createElement('span',{className:'label label-danger'},'bla bla') */}
        {/* tuong duong */}
        {/* <span class="label label-danger">bla bla</span> */}

        <div>
          <div className="panel panel-default">
            {colorButtons}
          </div>

          <h3
            style={{
              fontSize: this.state.text_size,
              color: this.state.text_color,
            }}
          >
            Id: {something.id} <br />
            Name: {something.name} <br />
            Value: {something.value} <br />
            Status:{something.status ? "Ready" : "Delay"}
          </h3>
          {this.showStatus(something)}

          <button
            type="button"
            className="btn btn-danger"
            onClick={this.onUpSize}
          >
            Up-size
          </button>
          <button
            type="button"
            className="btn btn-danger"
            onClick={this.onDownSize}
          >
            Down-Size
          </button>
        </div>
      </div>
    );
  }
}
export default Util;
