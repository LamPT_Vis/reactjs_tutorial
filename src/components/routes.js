import React, { Component } from "react";
import HelloReact from "../HelloReact";
import NotFound from "./NotFound";
import ProductDetail from "./ProductDetail";
import Login from './Login';
import TrackList from "./TrackList";
import TrackListReflect from "./TrackListReflect";

const routes = [
  {
    path: '/',
    isExact: true,
    main: () => <HelloReact />,
  },
  {
    path: '/login',
    isExact: false,
    main: () => <Login />,
  },
  {
    path: '/songs',
    isExact: false,
    main: () => <TrackList />,
  },
  {
    path: '/songsReflect',
    isExact: false,
    main: () => <TrackListReflect />,
  },
  {
    path: '/detail/:alt',
    isExact: false,
    main: ({match}) => <ProductDetail match={match} />,
  },
  {
    path: '',
    isExact: false,
    main: () => <NotFound />,
  },
];

export default routes;
