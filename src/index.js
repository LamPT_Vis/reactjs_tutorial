import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Navigator from "./Navigator";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import {configureStore} from './store';
import * as actions from './actions';

const tracks=[
  {
    id:1,
    title:"star sky"
  },
  {
    id:2,
    title:"lengend never die"
  }
];

const store=configureStore();
store.dispatch(actions.setTracks(tracks));

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Navigator />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
