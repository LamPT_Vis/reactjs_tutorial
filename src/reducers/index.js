import React from 'react';
import {combineReducers} from 'redux';
import tracks from "./tracks";

export default combineReducers({
    tracks
})