import React from 'react';
import {ActionType} from '../actions/constant';

const initialState=[];

export default function (state=initialState,action){
    switch(action.type){
        case ActionType.TRACKS_SET:{
            return setTracks(state,action);
        }
        case ActionType.TRACKS_ADD:{
            return addTracks(state,action);
        }
    }
    return state;
}

function setTracks(state,action){
    const {tracks}=action;
    return[...state,...tracks];
}

function addTracks(state,action){
    const trackTitle=action.title;
    const addItem=[
        {
            id:state.length+1,
            title:trackTitle
        }
    ];
    return[...state,...addItem];
}